package question1;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class Hand implements Serializable, Iterable<Card> {

    private static final long serialVersionUID = 102L;

    private LinkedList<Card> hand;

    public Hand() {
        hand = new LinkedList<>();
    };

    public void add(Card card) {

    }

    public void add(Collection<Card> cards) {

    }

    public void add(Hand otherHand) {

    }

    public boolean remove(Card removeCard) {
        // remove single card from list if present
        return true;
    }

    public boolean remove(Hand otherHand) {
        // remove all cards from other hand if present
        return true;
    }

    public Card remove(int cardPosition) {
        // return removed card
        return null;
    }

    public void sortDescending() {
        //hand.get(i).
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Iterator<Card> iterator() {
        return null;
    }
}
