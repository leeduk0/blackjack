package question1;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class Deck implements Serializable, Iterable<Card>{

    private static final long serialVersionUID = 112L;

    private LinkedList<Card> deck;

    public Deck() {
        newDeck();
    }

    public void newDeck() {
        if(deck == null) {
            deck = new LinkedList<>();
        } else {
            deck.clear();
        }

        for(Card.Suit s : Card.Suit.values()) {
            for(Card.Rank r : Card.Rank.values()) {
                deck.add(new Card(r, s));
            }
        }

    }

    public int size() {
        return deck.size();
    }

    public void shuffle() {
        Random rand = new Random();
        for (int i = 0; i < deck.size(); i++) {
            int randValue = rand.nextInt(52);
            Card swapCard = deck.get(randValue);
            deck.set(randValue, deck.get(i));
            deck.set(i, swapCard);
        }

    }

    public Card deal() {
        return deck.poll();
    }

    @Override
    public Iterator iterator() {
        return deck.iterator();
    }


    public class SecondCardIterator implements Iterator<Card> {

        private int nextIndex = 0;

        @Override
        public boolean hasNext() {
            return (nextIndex + 2 < size());
        }

        @Override
        public Card next() {
            Card retCard = deck.get(nextIndex);
            nextIndex += 2;
            return retCard;
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeInt(size());

        Iterator<Card> it = new SecondCardIterator();

        while(it.hasNext()) {
            out.writeObject(it.next());
        }
    }


}
