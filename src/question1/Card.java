package question1;

import java.io.Serializable;
import java.util.Comparator;

public class Card implements Serializable, Comparable<Card> {

    private static final long serialVersionUID = 111L;

    enum Rank {
        TWO(2),
        THREE(3),
        FOUR(4),
        FIVE(5),
        SIX(6),
        SEVEN(7),
        EIGHT(8),
        NINE(9),
        TEN(10),
        JACK(10),
        QUEEN(10),
        KING(10),
        ACE(11);

        private int value;

        Rank(int value) {
            this.value = value;
        }

        public Rank getPrevious() {
            return values()[(ordinal() - 1) % values().length];
        }

        public int getValue() {
            return this.value;
        }
    }

    enum Suit {
        CLUBS(1),
        DIAMONDS(2),
        HEARTS(3),
        SPADES(4);

        private int suitValue;

        Suit(int suitValue) {
            this.suitValue = suitValue;
        }

        public int getSuitValue() {
            return this.suitValue;
        }
    }

    private Rank rank;
    private Suit suit;

    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public static int sum(Card card1, Card card2) {
        return card1.getRank().getValue() + card2.getRank().getValue();
    }

    public static boolean isBlackjack(Card card1, Card card2) {
        return sum(card1, card2) == 21; // TODO CHECK
    }

    @Override
    public int compareTo(Card o) {
        if(this.rank == o.rank) {
            return this.suit.ordinal() - o.getSuit().ordinal();
        }
        return this.rank.ordinal() - o.getRank().ordinal();
    }

    public static class CompareAscending implements Comparator<Card> {

        @Override
        public int compare(Card o1, Card o2) {
            return -1 * o1.compareTo(o2);
        }
    }

    public static class CompareSuit implements Comparator<Card> {

        @Override
        public int compare(Card o1, Card o2) {
            if(o1.getSuit() == o2.getSuit()) {
                return o1.getRank().ordinal() - o2.getRank().ordinal();
            }
            return o1.getSuit().ordinal() - o2.getSuit().ordinal();
        }
    }

    @Override
    public String toString() {
        return null; // TODO: String builder
    }

}



